
import numpy as np
import pandas as pd
from flask import Flask,jsonify,request,render_template
from ANN import NeuralNetwork
app = Flask(__name__)
 
@app.route('/')
def index():
    return render_template("Index.html",status = "success");


@app.route('/test',methods=['POST'])
def test_model():
    data = pd.read_csv('static/dataset.csv').values
    N, d = data.shape
    X = data[:, 0:d-1].reshape(-1, d-1)
    y = data[:, 2].reshape(-1, 1)

    p = NeuralNetwork([X.shape[1], 2, 1], 0.1)
    p.fit(X, y, int (request.form["epoch"]), int (request.form["verbose"]))

    test = [int (request.form["salary"]),int (request.form["time"])]
    rs ="CHO VAY" if  p.predict(test)[0][0] > 0.5 else "KHÔNG CHO VAY"
    return jsonify(result=rs)

@app.route('/fit',methods=['POST'])
def fit_model():
    data = pd.read_csv('static/dataset.csv').values
    N, d = data.shape
    X = data[:, 0:d-1].reshape(-1, d-1)
    y = data[:, 2].reshape(-1, 1)

    p = NeuralNetwork([X.shape[1], 2, 1], 0.1)
    result = p.fit(X, y, int (request.form["epoch"]), int (request.form["verbose"]))
    return jsonify(status="success",result = result[0])

@app.route('/fitdemo',methods=['POST'])
def fit_demol():
    data = pd.read_csv('static/dataset1.csv').values
    N, d = data.shape
    X = data[:, 0:d-1].reshape(-1, d-1)
    y = data[:, 2].reshape(-1, 1)

    p = NeuralNetwork([X.shape[1], 2, 1], 0.1)
    result = p.fit(X, y, int (request.form["epoch"]), int (request.form["verbose"]))
    return jsonify(status="success",result = result[0],data=result[1],loss=result[2],verbose=int (request.form["verbose"]))

@app.route('/demo')
def demo():
    return render_template("demo.html")
 
if __name__ == '__main__':
    app.run()